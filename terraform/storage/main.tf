resource "aws_s3_bucket" "bucket-log" {
  bucket = "${var.s3-logs}"
  acl = "log-delivery-write"
}

resource "aws_s3_bucket" "bucket-artefacts" {
  bucket = "${var.s3-artefacts}"
  acl = "private"

  versioning {
    enabled = true
  }

  logging {
    target_bucket = "${aws_s3_bucket.bucket-log.id}"
    target_prefix = "log/"
  }
}
