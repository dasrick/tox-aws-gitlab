terraform {
  backend "s3" {
    encrypt = true
    bucket = "tox-state-storage"
    key    = "tox-esearch"
  }
}
