resource "aws_lambda_function" "esearch-matcher" {
  function_name = "tox-esearch-matcher"
  description = "eSearch Matcher"

  # The bucket name as created earlier with "aws s3api create-bucket"
  s3_bucket = "${var.s3-artefacts}"
  s3_key = "v1.0.0/esearch-matcher.zip"  // ToDo version into var

  # "main" is the filename within the zip file (main.js) and "handler"
  # is the name of the property under which the handler function was
  # exported in that file.
  handler = "esearch-matcher.handler"
  runtime = "nodejs8.10"

  role = "${aws_iam_role.role-lambda-exec.arn}"
}
